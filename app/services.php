<?php

$container = $app->getContainer();

/**
 * Database Abstraction
 */
$container['database'] = function ($c) {
    return new App\Classes\Database($c->db);
};

$container['services'] = function ($c) {
    return [
        'countries' => new App\Services\Countries($c->database),
        'states' => new App\Services\States($c->database),
        'counties' => new App\Services\Counties($c->database)
    ];
};