<?php

$app->get('/', 'App\Controller\DefaultController:getHelp');
$app->get('/version', 'App\Controller\DefaultController:getVersion');
$app->get('/status', 'App\Controller\DefaultController:getStatus');

$app->group('/api/v1', function () use ($app) {
	$app->group('/calculator', function () use ($app) {
		$app->get('', 'App\Controller\Calculator\Controller:GetAllStatistics');
		$app->get('/country-average-tax-rate', 'App\Controller\Calculator\Controller:getCountryAverageTaxRate');
		$app->get('/average-tax-rate-per-state', 'App\Controller\Calculator\Controller:getAllAverageCountyTaxPerState');
		$app->get('/overall-per-state/[{amount}]', 'App\Controller\Calculator\Controller:overallTaxAmountPerState');
		$app->get('/average-per-state/[{amount}]', 'App\Controller\Calculator\Controller:averageTaxAmountPerState');
		$app->get('/overall-country-taxes/[{amount}]', 'App\Controller\Calculator\Controller:overallCountryTaxes');
	});

	$app->group('/countries', function () use ($app) {
		$app->get('', 'App\Controller\Country\GetAllCountries');
		$app->get('/[{id}]', 'App\Controller\Country\GetOneCountry');
		$app->post('', 'App\Controller\Country\AddRecord');
		$app->put('/[{id}]', 'App\Controller\Country\UpdateCountry');
		$app->delete('/[{id}]', 'App\Controller\Country\DeleteCountry');
	});

	$app->group('/states', function () use ($app) {
		$app->get('', 'App\Controller\State\GetAllStates');
		$app->get('/[{id}]', 'App\Controller\State\GetOneState');
		$app->post('', 'App\Controller\State\AddRecord');
		$app->put('/[{id}]', 'App\Controller\State\UpdateState');
		$app->delete('/[{id}]', 'App\Controller\State\DeleteState');
	});

	$app->group('/counties', function () use ($app) {
		$app->get('', 'App\Controller\County\GetAllCounties');
		$app->get('/[{id}]', 'App\Controller\County\GetOneCounty');
		$app->post('', 'App\Controller\County\AddRecord');
		$app->put('/[{id}]', 'App\Controller\County\UpdateCounty');
		$app->delete('/[{id}]', 'App\Controller\County\DeleteCounty');
	});
});

// /* Contries Route */
// $app->get('/countries', function (Request $request, Response $response, array $args) {
//     $data = $this->models['countries']->getAll();
//     return $response->withJson($data);
// });

// $app->get('/countries/{id}', function (Request $request, Response $response, array $args) {
//     $data = $this->models['countries']->getById($args['id']);
//     return $response->withJson($data);
// });

// /* States Route */
// $app->get('/states', function (Request $request, Response $response, array $args) {
//     $data = $this->models['states']->getAll();
//     return $response->withJson($data);
// });

// /* Counties Route */
// $app->get('/counties', function (Request $request, Response $response, array $args) {
//     $data = $this->models['counties']->getAll();
//     return $response->withJson($data);
// });

// $app->get('/calculator', function (Request $request, Response $response, array $args) {
//     var_dump($this->calculator->init());
// });