<?php

return [
    'settings' => [
        'displayErrorDetails'    => true,
        'addContentLengthHeader' => false,
        'logger'                 => [
            'enabled' => true,
            'name'    => 'slim-app',
            'path'    => __DIR__ . '/../logs/app.log',
            'level'   => \Monolog\Logger::INFO,
        ],
        'db'                     => [
            'host'   => 'localhost',
            'user'   => 'root',
            'pass'   => '123456789',
            'dbname' => 'api_service',
        ],
    ],
];
