<?php

use Phinx\Migration\AbstractMigration;

class InitialApiMigration extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $countries = $this->table('countries');
        $countries->addColumn('name', 'string', ['limit' => '150'])
            ->addColumn('iso', 'string', ['limit' => '3'])
            ->addColumn('created', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('updated', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->create();

        $states = $this->table('states');
        $states->addColumn('name', 'string', ['limit' => '150'])
            ->addColumn('country_id', 'integer')
            ->addColumn('created', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('updated', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->create();

        $counties = $this->table('counties');
        $counties->addColumn('name', 'string', ['limit' => '150'])
            ->addColumn('tax_rate', 'integer')
            ->addColumn('state_id', 'integer')
            ->addColumn('created', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('updated', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->create();
    }
}

/**
 * The logic here is simple. This should be hierarchy:
 * 1. Country
 * 2. State
 * 3. County
 *
 * Input via the API REST to calculate each and every tax, based on the tax rate, so e.g. if I 
 * place 10000 $ it should give me the information about the requirement
 */
