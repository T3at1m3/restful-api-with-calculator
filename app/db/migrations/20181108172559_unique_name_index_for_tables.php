<?php

use Phinx\Migration\AbstractMigration;

class UniqueNameIndexForTables extends AbstractMigration
{
    public function change()
    {
        $countries = $this->table('countries');
        $countries->addIndex(['name'], [
            'unique' => true,
            'name'   => 'idx_country_name'])
            ->save();

        $states = $this->table('states');
        $states->addIndex(['name'], [
            'unique' => true,
            'name'   => 'idx_state_name'])
            ->save();

        $counties = $this->table('counties');
        $counties->addIndex(['name'], [
            'unique' => true,
            'name'   => 'idx_county_name'])
            ->save();
    }
}
