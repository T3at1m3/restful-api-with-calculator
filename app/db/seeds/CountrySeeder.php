<?php

use Phinx\Seed\AbstractSeed;

class CountrySeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'name'    => 'Bulgaria',
                'iso'     => 'BG',
                'created' => date('Y-m-d H:i:s'),
                'updated' => date('Y-m-d H:i:s'),
            ],
        ];

        $countries = $this->table('countries');
        $countries->insert($data)
            ->save();
    }
}
