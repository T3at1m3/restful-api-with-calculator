<?php

use Phinx\Seed\AbstractSeed;

class CountySeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'name'     => 'Vratsa',
                'tax_rate' => 10,
                'state_id' => 1,
                'created'  => date('Y-m-d H:i:s'),
                'updated'  => date('Y-m-d H:i:s'),
            ], [
                'name'     => 'Vidin',
                'tax_rate' => 8,
                'state_id' => 1,
                'created'  => date('Y-m-d H:i:s'),
                'updated'  => date('Y-m-d H:i:s'),
            ], [
                'name'     => 'Lom',
                'tax_rate' => 17,
                'state_id' => 1,
                'created'  => date('Y-m-d H:i:s'),
                'updated'  => date('Y-m-d H:i:s'),
            ], [
                'name'     => 'Veliko Tarnovo',
                'tax_rate' => 18,
                'state_id' => 2,
                'created'  => date('Y-m-d H:i:s'),
                'updated'  => date('Y-m-d H:i:s'),
            ], [
                'name'     => 'Ruse',
                'tax_rate' => 15,
                'state_id' => 2,
                'created'  => date('Y-m-d H:i:s'),
                'updated'  => date('Y-m-d H:i:s'),
            ], [
                'name'     => 'Gabrovo',
                'tax_rate' => 11,
                'state_id' => 2,
                'created'  => date('Y-m-d H:i:s'),
                'updated'  => date('Y-m-d H:i:s'),
            ], [
                'name'     => 'Pleven',
                'tax_rate' => 18,
                'state_id' => 2,
                'created'  => date('Y-m-d H:i:s'),
                'updated'  => date('Y-m-d H:i:s'),
            ], [
                'name'     => 'Varna',
                'tax_rate' => 12,
                'state_id' => 3,
                'created'  => date('Y-m-d H:i:s'),
                'updated'  => date('Y-m-d H:i:s'),
            ], [
                'name'     => 'Dobrich',
                'tax_rate' => 11,
                'state_id' => 3,
                'created'  => date('Y-m-d H:i:s'),
                'updated'  => date('Y-m-d H:i:s'),
            ], [
                'name'     => 'Shumen',
                'tax_rate' => 11,
                'state_id' => 3,
                'created'  => date('Y-m-d H:i:s'),
                'updated'  => date('Y-m-d H:i:s'),
            ], [
                'name'     => 'Sofia Capitol',
                'tax_rate' => 20,
                'state_id' => 4,
                'created'  => date('Y-m-d H:i:s'),
                'updated'  => date('Y-m-d H:i:s'),
            ], [
                'name'     => 'Blagoevgrad',
                'tax_rate' => 15,
                'state_id' => 4,
                'created'  => date('Y-m-d H:i:s'),
                'updated'  => date('Y-m-d H:i:s'),
            ], [
                'name'     => 'Bansko',
                'tax_rate' => 19,
                'state_id' => 4,
                'created'  => date('Y-m-d H:i:s'),
                'updated'  => date('Y-m-d H:i:s'),
            ], [
                'name'     => 'Plovdiv',
                'tax_rate' => 17,
                'state_id' => 5,
                'created'  => date('Y-m-d H:i:s'),
                'updated'  => date('Y-m-d H:i:s'),
            ], [
                'name'     => 'Velingrad',
                'tax_rate' => 11,
                'state_id' => 5,
                'created'  => date('Y-m-d H:i:s'),
                'updated'  => date('Y-m-d H:i:s'),
            ], [
                'name'     => 'Pazardjik',
                'tax_rate' => 12,
                'state_id' => 5,
                'created'  => date('Y-m-d H:i:s'),
                'updated'  => date('Y-m-d H:i:s'),
            ], [
                'name'     => 'Burgas',
                'tax_rate' => 20,
                'state_id' => 6,
                'created'  => date('Y-m-d H:i:s'),
                'updated'  => date('Y-m-d H:i:s'),
            ], [
                'name'     => 'Haskovo',
                'tax_rate' => 5,
                'state_id' => 6,
                'created'  => date('Y-m-d H:i:s'),
                'updated'  => date('Y-m-d H:i:s'),
            ], [
                'name'     => 'Sliven',
                'tax_rate' => 15,
                'state_id' => 6,
                'created'  => date('Y-m-d H:i:s'),
                'updated'  => date('Y-m-d H:i:s'),
            ],
        ];

        $counties = $this->table('counties');
        $counties->insert($data)
            ->save();
    }
}
