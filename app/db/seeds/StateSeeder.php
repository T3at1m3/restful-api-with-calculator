<?php

use Phinx\Seed\AbstractSeed;

class StateSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        /**
         * In Bulgaria we don't have states, but we have regions. This will work.
         * They belong to a country and have countes, so for now let's name them states
         * We have 6 regions, so no matter - I'll make one more record
         */
        $data = [
            [
                'name'       => 'North West',
                'country_id' => 1,
                'created'    => date('Y-m-d H:i:s'),
                'updated'    => date('Y-m-d H:i:s'),
            ], [
                'name'       => 'North Central',
                'country_id' => 1,
                'created'    => date('Y-m-d H:i:s'),
                'updated'    => date('Y-m-d H:i:s'),
            ], [
                'name'       => 'North East',
                'country_id' => 1,
                'created'    => date('Y-m-d H:i:s'),
                'updated'    => date('Y-m-d H:i:s'),
            ], [
                'name'       => 'South West',
                'country_id' => 1,
                'created'    => date('Y-m-d H:i:s'),
                'updated'    => date('Y-m-d H:i:s'),
            ], [
                'name'       => 'South Central',
                'country_id' => 1,
                'created'    => date('Y-m-d H:i:s'),
                'updated'    => date('Y-m-d H:i:s'),
            ], [
                'name'       => 'South East',
                'country_id' => 1,
                'created'    => date('Y-m-d H:i:s'),
                'updated'    => date('Y-m-d H:i:s'),
            ],
        ];

        $states = $this->table('states');
        $states->insert($data)
            ->save();
    }
}
