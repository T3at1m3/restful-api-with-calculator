<?php

namespace App\Services;

class Counties extends BaseService
{
    /**
     * Name of the table in the database
     * @var string
     */
    private $tableName = 'counties';

    /**
     * Return all results from the table
     * @return array
     */
    public function getAll()
    {
        return $this->select($this->tableName);
    }

    /**
     * Return row by ID
     * @param  integer $id PK for table
     * @return array
     */
    public function getById($id)
    {
        return $this->select($this->tableName, [], "WHERE id = {$id}");
    }

    /**
     * Gets data based on params
     * @param  array $params Which columns to return
     * @param  string $where Where statement
     * @return array
     */
    public function get(array $params = [], string $where = '')
    {
        if (empty($params) && empty($where)) {
            return $this->getAll();
        }

        return $this->select($this->tableName, $params, $where);
    }

        /**
     * Creates a new record
     *
     * @param array $input
     */
    public function addCounty($input)
    {
        $status = $this->create($this->tableName, $input);

        if ($status) {
            return sprintf("The record was successfuly created with id: %s", $status);
        }
        return null;
    }

    /**
     * Update a record
     *
     * @param  array $input
     * @param  string $id
     * @return array
     */
    public function updateCounty($input, $id)
    {
        $status = $this->update($this->tableName, $input, $id);

        return ($status) ? [
            'code' => 200,
            'message' => 'Record Updated!'
        ] : [
            'code' => 204,
            'message' => 'No such record.'
        ];
    }

    /**
     * Delete
     *
     * @param  integer $id
     * @return array
     */
    public function deleteCounty($id)
    {
        $status = $this->delete($this->tableName, $id);

        return ($status) ? [
            'code' => 200,
            'message' => 'Record Deleted!'
        ] : [
            'code' => 204,
            'message' => 'No such record.'
        ];
    }
}
