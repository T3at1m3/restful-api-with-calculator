<?php

namespace App\Services;

use App\Classes\Database;

class BaseService
{
    private $database;

    public function __construct(Database $database)
    {
        $this->database = $database;
    }

    /**
     * Creates a new entry into the DB
     * @param  string $table
     * @param  array  $params
     * @return string         Id for the last insert
     */
    public function create($table, $params)
    {
        if (empty($table)) {
            throw new \Exception("BaseService class Critical Error: No table specified.");
        }

        if (empty($params)) {
            throw new \Exception("BaseService class Critical Error: No params present - nothing to insert.");
        }

        $fields = implode(', ', array_keys($params));
        $values = ':' . implode(', :', array_keys($params));

        $this->database->prepare(
            "INSERT INTO {$table}
            ({$fields})
            VALUES
            ({$values})
            ",
            $params
        )->execute();

        return $this->database->lastInsertID();
    }

    /**
     * Selects all results from the Database that matches the variables
     *
     * @param  string $table  The table name
     * @param  array  $params
     * @param  string $where  Additonal Condition
     * @return array
     */
    public function select($table, array $params = [], string $where = '')
    {
        if (empty($table)) {
            throw new \Exception("BaseService Critical Error: No table specified.");
        }

        if (!empty($params)) {
            end($params);
            $lastElementKey = key($params);
            $columns        = '';
            foreach ($params as $key => $value) {
                $columns .= "`{$table}`.{$value}";
                if ($key !== $lastElementKey) {
                    $columns .= ', ';
                }
            }
        } else {
            $columns = '*';
        }

        $statement = $this->database->prepare("
            SELECT {$columns} FROM {$table} {$where}
            ")->execute();

        return $statement->fetchAllAssoc();
    }

    /**
     * Update a record
     * @param  string $table  Table name
     * @param  array  $params
     * @param  integer $id
     * @return integer
     */
    public function update(string $table, array $params, $id)
    {
        $update = '';
        end($params);
        $last = key($params);

        foreach ($params as $key => $value) {
            $update .= sprintf("%s = :%s", $key, $key);
            if ($key != $last) {
                $update .= ', ';
            }
        }

        $this->database->prepare(
            "UPDATE {$table}
            SET
                {$update}
            WHERE
                id = {$id}
            ",
            $params
        )->execute();

        return $this->database->rowCount();
    }

    /**
     * Delete a record
     * @param  string $table
     * @param  integer $id
     * @return integer
     */
    public function delete(string $table, $id)
    {
        $sql = "DELETE FROM {$table}
                WHERE id = :id";
        $params = [
            'id' => $id,
        ];

        $this->database->prepare($sql, $params)->execute();

        return $this->database->rowCount();
    }

    /**
     * Counts the affected rows from the last query
     *
     * @return integer
     */
    public function rowCount()
    {
        return $this->database->rowCount();
    }
}
