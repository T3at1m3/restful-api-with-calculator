<?php

namespace App\Classes;

class Database
{
    /**
     * PDO
     * @var object
     */
    private $pdo;
    private $stmt;
    private $sql;
    private $params;

    public function __construct(\PDO $pdo)
    {
        if (empty($pdo)) {
            throw new Exception("Database requires PDO driver", 500);
        }
        $this->pdo = $pdo;
    }

    /**
     * The prepared statement to be executed by the PDO object
     * @param  string $sql    SQL statement
     * @param  array  $params Prepared parameters needed for the prepared
     *                        statement
     * @return object         The PDO object
     */
    public function prepare($sql, $params = array())
    {
        $this->stmt   = $this->pdo->prepare($sql);
        $this->params = $params;
        $this->sql    = $sql;
        return $this;
    }

    /**
     * The execute method, executes the prepared statement
     * @return object The PDO object
     */
    public function execute()
    {
        $this->stmt->execute($this->params);
        return $this;
    }

    /**
     * Returns the results of the executed query as an associative array
     * @param  boolean $assoc If the param is false, returns the array with
     *                        numerical values as keys
     * @return array
     */
    public function fetchAllAssoc($assoc = true)
    {
        if ($assoc) {
            return $this->stmt->fetchAll(\PDO::FETCH_ASSOC);
        } else {
            return $this->stmt->fetchAll(\PDO::FETCH_NUM);
        }
    }

    /**
     * Returns the result of the single executed query as an associative array
     * @return array
     */
    public function fetchRowAssoc($assoc = true)
    {
        if ($assoc) {
            return $this->stmt->fetch(\PDO::FETCH_ASSOC);
        } else {
            return $this->stmt->fetch(\PDO::FETCH_NUM);
        }

    }

    /**
     * Fetches the result based on the $column parameter
     * @param  string $column The desired column from a table in SQL
     * @return array         This array holds all the column data
     */
    public function fetchAllColumn($column)
    {
        return $this->stmt->fetchAll(\PDO::FETCH_COLUMN, $column);
    }

    /**
     * Fetches a single row of the selected column
     * @param  string $column The desired column from a table in SQL
     * @return array         The requested row
     */
    public function fetchRowColumn($column)
    {
        return $this->stmt->fetch(\PDO::FETCH_COLUMN, $column);
    }

    /**
     * The last inserted ID of an INSERT SQL query
     * @return string
     */
    public function lastInsertID()
    {
        return $this->pdo->lastInsertId();
    }

    /**
     * Counts the affected rows from the last query
     * @return integer The row count
     */
    public function rowCount()
    {
        return $this->stmt->rowCount();
    }
}