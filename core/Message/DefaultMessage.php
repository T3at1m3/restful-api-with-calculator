<?php

namespace App\Message;

/**
 * Default Message.
 */
abstract class DefaultMessage
{
    const API_VERSION = 'ver. 0.1 beta';
}
