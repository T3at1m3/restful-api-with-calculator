<?php

namespace App\Controller\Country;

use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Get One Country
 */
class GetOneCountry extends Controller
{
    /**
     * Get one by id
     *
     * @param  Request  $request
     * @param  Response $response
     * @param  array    $args
     * @return Response
     */
    public function __invoke(Request $request, Response $response, array $args)
    {
        $this->setParams($request, $response, $args);
        $result = $this->getDefaultService()->getById($this->args['id']);

        if (empty($result)) {
        	$code = 404;
        	$status = 'not found';
        	$result = 'The requested resource was not found.';
        } else {
        	$code = 200;
        	$status = 'success';
        }

        return $this->jsonResponse($status, $result, $code);
    }
}
