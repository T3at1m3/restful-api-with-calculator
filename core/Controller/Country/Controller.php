<?php

namespace App\Controller\Country;

use App\Controller\BaseController;
use App\Services\Countries;
use Slim\Container;


abstract class Controller extends BaseController
{
	public function __construct(Container $container)
	{
		$this->logger = $container->get('logger');
		$this->defaultService = $container->get('services')['countries'];
	}

    /**
     * @return TaskService
     */
    protected function getDefaultService()
    {
        return $this->defaultService;
    }

    /**
     * @return array
     */
    protected function getInput()
    {
        return $this->request->getParsedBody();
    }
}