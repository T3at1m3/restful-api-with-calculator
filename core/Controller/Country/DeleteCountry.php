<?php

namespace App\Controller\Country;

use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Delete a Country
 */
class DeleteCountry extends Controller
{
    /**
     * Delete a country
     *
     * @param  Request  $request
     * @param  Response $response
     * @param  array    $args
     * @return Response
     */
    public function __invoke(Request $request, Response $response, array $args)
    {
        $this->setParams($request, $response, $args);
        $result = $this->getDefaultService()->deleteCountry($this->args['id']);

        return $this->jsonResponse('success', $result['message'], $result['code']);
    }
}