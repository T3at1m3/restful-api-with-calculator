<?php

namespace App\Controller\Country;

use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Update Country Controller
 */
class UpdateCountry extends Controller
{
    /**
     * Update a country
     *
     * @param  Request  $request
     * @param  Response $response
     * @param  array    $args
     * @return Response
     */
    public function __invoke(Request $request, Response $response, array $args)
    {
        $this->setParams($request, $response, $args);
        $input  = $request->getParsedBody();
        $result = $this->getDefaultService()->updateCountry($input, $this->args['id']);

        return $this->jsonResponse('success', $result['message'], $result['code']);
    }
}
