<?php

namespace App\Controller\Country;

use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Add Country Controller
 */
class AddRecord extends Controller
{
    /**
     * Add a country
     *
     * @param  Request  $request
     * @param  Response $response
     * @param  array    $args
     * @return Response
     */
    public function __invoke(Request $request, Response $response, array $args)
    {
        $this->setParams($request, $response, $args);
        $input = $request->getParsedBody();
        $result = $this->getDefaultService()->addCountry($input);

        return $this->jsonResponse('success', $result, 201);
    }
}