<?php

namespace App\Controller\County;

use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Add County Controller
 */
class AddRecord extends Controller
{
    /**
     * Add a county
     *
     * @param  Request  $request
     * @param  Response $response
     * @param  array    $args
     * @return Response
     */
    public function __invoke(Request $request, Response $response, array $args)
    {
        $this->setParams($request, $response, $args);
        $input = $request->getParsedBody();
        $result = $this->getDefaultService()->addCounty($input);

        return $this->jsonResponse('success', $result, 201);
    }
}