<?php

namespace App\Controller\County;

use App\Controller\BaseController;
use App\Services\Counties;
use Slim\Container;

/**
 * County Controller
 */
abstract class Controller extends BaseController
{
	public function __construct(Container $container)
	{
		$this->logger = $container->get('logger');
		$this->defaultService = $container->get('services')['counties'];
	}

    /**
     * @return TaskService
     */
    protected function getDefaultService()
    {
        return $this->defaultService;
    }

    /**
     * @return array
     */
    protected function getInput()
    {
        return $this->request->getParsedBody();
    }
}