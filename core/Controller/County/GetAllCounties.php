<?php

namespace App\Controller\County;

use Slim\Http\Request;
use Slim\Http\Response;

class GetAllCounties extends Controller
{
	/**
     * Get all counties.
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
	public function __invoke(Request $request, Response $response, array $args)
	{
		$this->setParams($request, $response, $args);
		$result = $this->getDefaultService()->getAll();

        return $this->jsonResponse('success', $result, 200);
	}
}