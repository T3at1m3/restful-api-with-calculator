<?php

namespace App\Controller\County;

use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Update County Controller
 */
class UpdateCounty extends Controller
{
    /**
     * Update a county
     *
     * @param  Request  $request
     * @param  Response $response
     * @param  array    $args
     * @return Response
     */
    public function __invoke(Request $request, Response $response, array $args)
    {
        $this->setParams($request, $response, $args);
        $input  = $request->getParsedBody();
        $result = $this->getDefaultService()->updateCounty($input, $this->args['id']);

        return $this->jsonResponse('success', $result['message'], $result['code']);
    }
}
