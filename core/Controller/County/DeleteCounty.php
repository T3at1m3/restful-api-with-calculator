<?php

namespace App\Controller\County;

use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Delete a County
 */
class DeleteCounty extends Controller
{
    /**
     * Delete a county
     *
     * @param  Request  $request
     * @param  Response $response
     * @param  array    $args
     * @return Response
     */
    public function __invoke(Request $request, Response $response, array $args)
    {
        $this->setParams($request, $response, $args);
        $result = $this->getDefaultService()->deleteCounty($this->args['id']);

        return $this->jsonResponse('success', $result['message'], $result['code']);
    }
}