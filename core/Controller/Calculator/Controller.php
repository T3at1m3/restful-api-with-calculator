<?php

namespace App\Controller\Calculator;

use App\Controller\BaseController;
use App\Services\Countries;
use App\Services\Counties;
use App\Services\States;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Calculator Controller
 */
class Controller extends BaseController
{
    /**
     * @var Container Services $services
     */
    protected $services;

	public function __construct(Container $container)
	{
		$this->logger = $container->get('logger');
        $this->services = $container->get('services');
	}

    public function getAllStatistics($request, $response, $args)
    {
        $this->setParams($request, $response, $args);
        $url = 'api/v1/calculator/';
        $message = [
            'Method' => __FUNCTION__,
            'routes' => [
                'notice' => 'You can check routes.php for the api routes',
                'Output the average tax rate of the country' => $url . 'country-average-tax-rate',
                'Output the average county tax rate per state' => $url . 'average-tax-rate-per-state',
                'Output the overall amount of taxes collected per state' => $url . 'overall-per-state/{amount}',
                'Output the average amount of taxes collected per state' => $url . 'average-per-state/{amount}',
                'Output the collected overall taxes of the country' => $url . 'overall-country-taxes/{amount}',
            ]
        ];
        return $this->jsonResponse('success', $message, 200);
    }

    public function getCountryAverageTaxRate($request, $response, $args)
    {
        $this->setParams($request, $response, $args);
        $countiesModel = $this->services['counties'];

        $getAllCounties = $countiesModel->getAll();

        $totalTaxRate = 0;
        foreach ($getAllCounties as $key => $value) {
            $totalTaxRate += $value['tax_rate'];
        }
        $totalResults = $countiesModel->rowCount();

        $averageTaxRate = ($totalTaxRate / $totalResults);

        $result = [
            'Average tax rate of the country' => $averageTaxRate
        ];

        return $this->jsonResponse('success', $result, 200);
    }

    public function getAllAverageCountyTaxPerState($request, $response, $args)
    {
        $this->setParams($request, $response, $args);
        $result = $this->getData();

        foreach ($result as $key => $state) {
            $result[$key]['average_tax_rate'] = $this->getStateAverageTax($state);
        }

        return $this->jsonResponse('success', $result, 200);
    }

    public function overallTaxAmountPerState($request, $response, $args)
    {
        $this->setParams($request, $response, $args);

        if (!is_numeric($args['amount'])) {
            throw new \Exception("Amount should be a numeric value");
        }

        $amount = $args['amount'];
        $result = $this->getData();
        $states = [];

        foreach ($result as $key => $state) {
            $states[$key]['name'] = $state['name'];
            $states[$key]['total_tax_rate'] = $this->getStateTotalTax($state);
            $states[$key]['total_tax'] = ($amount / 100) * $states[$key]['total_tax_rate'];
        }

        return $this->jsonResponse('success', $states, 200);
    }

    public function averageTaxAmountPerState($request, $response, $args)
    {
        $this->setParams($request, $response, $args);

        if (!is_numeric($args['amount'])) {
            throw new \Exception("Amount should be a numeric value");
        }
        $amount = $args['amount'];
        $result = $this->getData();
        $states = [];

        foreach ($result as $key => $state) {
            $states[$key]['name'] = $state['name'];
            $states[$key]['average_tax_rate'] = $this->getStateAverageTax($state);
            $states[$key]['average_tax'] = ($amount / 100) * $states[$key]['average_tax_rate'];
        }

        return $this->jsonResponse('success', $states, 200);
    }

    public function overallCountryTaxes($request, $response, $args)
    {
        $this->setParams($request, $response, $args);

        if (!is_numeric($args['amount'])) {
            throw new \Exception("Amount should be a numeric value");
        }
        $amount = $args['amount'];
        $result = $this->getData();
        $states = [];

        foreach ($result as $key => $state) {
            $states[$key]['name'] = $state['name'];
            $states[$key]['total_tax_rate'] = $this->getStateTotalTax($state);
            $states[$key]['total_tax'] = ($amount / 100) * $states[$key]['total_tax_rate'];
        }

        $totalCountryTax = 0;
        foreach ($states as $key => $value) {
            $totalCountryTax += $value['total_tax'];
        }

        $message = [
            'Total Country Income' => $totalCountryTax
        ];

        return $this->jsonResponse('success', $message, 200);
    }


    /**
     * Prepares data needed for the result
     * @param  array $states
     * @param  array $counties
     * @return array
     */
    private function prepareStateData($states, $counties)
    {
        $result = [];

        foreach ($states as $sk => $state) {
            $result[$sk] = $state;
            foreach ($counties as $ck => $county) {
                if ($county['state_id'] === $state['id']) {
                    $result[$sk]['counties'][] = $county;
                }
            }
        }

        return $result;
    }

    /**
     * Get data from services
     *
     * @return array
     */
    private function getData()
    {
        $statesData = $this->services['states']->getAll();
        $countiesData = $this->services['counties']->getAll();

        $result = $this->prepareStateData($statesData, $countiesData);
        return $result;
    }

    /**
     * Calculate state average tax
     * @param  array $state
     * @return float
     */
    private function getStateAverageTax($state)
    {
        $countiesCount = count($state['counties']);
        $totalTaxRate = 0;
        foreach ($state['counties'] as $key => $county) {
            $totalTaxRate += $county['tax_rate'];
        }

        return ($totalTaxRate / $countiesCount);
    }

    private function getStateTotalTax($state)
    {
        $totalTaxRate = 0;
        foreach ($state['counties'] as $key => $county) {
            $totalTaxRate += $county['tax_rate'];
        }

        return $totalTaxRate;
    }
}