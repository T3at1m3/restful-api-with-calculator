<?php

namespace App\Controller\State;

use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Update State Controller
 */
class UpdateState extends Controller
{
    /**
     * Update a State
     *
     * @param  Request  $request
     * @param  Response $response
     * @param  array    $args
     * @return Response
     */
    public function __invoke(Request $request, Response $response, array $args)
    {
        $this->setParams($request, $response, $args);
        $input  = $request->getParsedBody();
        $result = $this->getDefaultService()->updateState($input, $this->args['id']);

        return $this->jsonResponse('success', $result['message'], $result['code']);
    }
}
