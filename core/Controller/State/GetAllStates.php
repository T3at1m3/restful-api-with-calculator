<?php

namespace App\Controller\State;

use Slim\Http\Request;
use Slim\Http\Response;

class GetAllStates extends Controller
{
	/**
     * Get all States.
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
	public function __invoke(Request $request, Response $response, array $args)
	{
		$this->setParams($request, $response, $args);
		$result = $this->getDefaultService()->getAll();

        return $this->jsonResponse('success', $result, 200);
	}
}