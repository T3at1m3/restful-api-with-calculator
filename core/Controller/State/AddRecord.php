<?php

namespace App\Controller\State;

use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Add State Controller
 */
class AddRecord extends Controller
{
    /**
     * Add a State
     *
     * @param  Request  $request
     * @param  Response $response
     * @param  array    $args
     * @return Response
     */
    public function __invoke(Request $request, Response $response, array $args)
    {
        $this->setParams($request, $response, $args);
        $input = $request->getParsedBody();
        $result = $this->getDefaultService()->addState($input);

        return $this->jsonResponse('success', $result, 201);
    }
}