<?php

namespace App\Controller\State;

use App\Controller\BaseController;
use App\Services\States;
use Slim\Container;

/**
 * State Controller
 */
abstract class Controller extends BaseController
{
	public function __construct(Container $container)
	{
		$this->logger = $container->get('logger');
		$this->defaultService = $container->get('services')['states'];
	}

    /**
     * @return TaskService
     */
    protected function getDefaultService()
    {
        return $this->defaultService;
    }

    /**
     * @return array
     */
    protected function getInput()
    {
        return $this->request->getParsedBody();
    }
}