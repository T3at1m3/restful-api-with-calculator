<?php

namespace App\Controller\State;

use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Delete a State
 */
class DeleteState extends Controller
{
    /**
     * Delete a State
     *
     * @param  Request  $request
     * @param  Response $response
     * @param  array    $args
     * @return Response
     */
    public function __invoke(Request $request, Response $response, array $args)
    {
        $this->setParams($request, $response, $args);
        $result = $this->getDefaultService()->deleteState($this->args['id']);

        return $this->jsonResponse('success', $result['message'], $result['code']);
    }
}