# Slim Rest API

Small API REST Service for JobLeads GmbH

## Getting Stated

These instructions will get you a running service

### Installation

Get composer and install it on your OS ([Composer](https://getcomposer.org/))

Use composer in CMD

```bash
# Get the latest version
$ composer self-update
# Update the dependencies
$ composer update
# Dump the autoloader
$ composer dump-autoload -o
```

### Installing Phinx and Seeding Data

After you've downloaded Phinx
You can eather go
```bash
$ vendor/bin/phinx init
```
or manually edit the
```
<project-install-dir>/api/phinx.yaml
```
Place the required credentials for the DB
```yaml
	paths:
	    migrations: '%%PHINX_CONFIG_DIR%%/app/db/migrations'
	    seeds: '%%PHINX_CONFIG_DIR%%/app/db/seeds'

	environments:
	    default_migration_table: phinxlog
	    default_database: api_service

	    development:
	        adapter: mysql
	        host: localhost
	        name: api_service
	        user: <your-desired-username>
	        pass: '<your-desired-pass>' // mind the quotes
	        port: 3306
	        charset: utf8

	version_order: creation
```

You can use [Phinx Commands](http://docs.phinx.org/en/latest/commands.html) to create your default migration simply by running:

```bash
# Migrate the development environment
$ vendor/bin/phinx migrate -e development
# Seeders (They can be used in one line but haven't tested it)
$ vendor/bin/phinx seed:run -s CountrySeeder -e development
$ vendor/bin/phinx seed:run -s StateSeeder -e development
$ vendor/bin/phinx seed:run -s CountySeeder -e development
```
Now you have the DB all set up.

### Stuff to do with this

Checkout routes for the routes.
The calculator has the URLs mapped.

## Built With

* [Composer](https://getcomposer.org/) - Dependency Management
* [Phinx](http://docs.phinx.org/en/latest/index.html) - The little migration & seed magician
* [ROME](https://rometools.github.io/rome/) - Used to generate RSS Feeds

## Authors

* **Alex Takev** - *Initial work* - [MyLittle Blog](https://t3at1m3.wordpress.com/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
